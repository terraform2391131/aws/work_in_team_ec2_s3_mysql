variable "aws_region"{
    description = "AWS server region"
    type = string
    default = "eu-central-1"
}

variable "project_short_name" {
    description = "Project ShortName for project variables"
    type = string
    default = "project1"
}

provider "aws" {
    profile = "default"
    region = var.aws_region
}