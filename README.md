# Work_in_team_EC2_S3_Mysql
<br>
What's included:<br>
EC2 instance<br>
Mysql database<br>
S3bucket for terraform.tfstate<br>
terraform.tfstate remote file locking<br>

# Edit settings in "terraform.tfvars" file


# Before first use terraform needs first to create S3 bucket for backend: <br>
so you need first to comment all "aws_terraform_backend.tf" file and run<br>
terraform init<br>
terraform apply<br>
then uncomment "aws_terraform_backend.tf"<br>
terraform init<br>
terraform apply<br>

# Later to remove this resource you have to:<br>
comment all "aws_terraform_backend.tf" file<br>
terraform init<br>
terraform apply<br>
you are again with local terraform.tfstate, but you need to clear S3 bucket:<br>
comment all lifecycle in "aws_s3.tf" file<br>
comment "aws_s3_bucket_versioning" resource <br>
add/uncomment "force_destroy = true" in "aws_s3.tf" file<br>
terraform apply<br>
terraform destroy<br>