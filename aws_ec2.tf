variable "ec2_instance_ami" {
  description = "AMI of EC2 instance"
  type        = string
  default     = "ami-061afb021813a8927"
}

variable "ec2_instance_type" {
  description = "Type of EC2 instance"
  type        = string
  default     = "t3.micro"
}

# 3 resources down toggether, create keypair
resource "aws_key_pair" "TF_aws_public_key" {
  key_name   = join("_", ["TF", var.project_short_name, "aws_public_key"])
  public_key = tls_private_key.rsa.public_key_openssh
}

resource "tls_private_key" "rsa" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "local_file" "TF_aws_private_key" {
  content  = tls_private_key.rsa.private_key_pem
  filename = join("_", ["TF", var.project_short_name, "aws_private_key.pem"])
  file_permission = 0400
}

# create ec2 instance
resource "aws_instance" "aws_web" {
  ami           = var.ec2_instance_ami
  instance_type = var.ec2_instance_type
  count = 1
  security_groups = [aws_security_group.TF_SG_FOR_EC2.name]
 
  key_name = join("_", ["TF", var.project_short_name, "aws_public_key"])

  tags = {
    Name = join("-", ["EC2", var.project_short_name])
  }
}



output "ec2_public_ip" {
  value = aws_instance.aws_web[*].public_ip
  description = "The public IP address of the instance."
}