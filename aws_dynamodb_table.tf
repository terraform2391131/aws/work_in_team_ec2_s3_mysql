resource "aws_dynamodb_table" "aws_dynamodb_table_lock" {
    name = "aws_dynamodb_state_locking"
    billing_mode = "PAY_PER_REQUEST"
    hash_key = "LockID"

    attribute {
        name = "LockID"
        type = "S"
    }
}