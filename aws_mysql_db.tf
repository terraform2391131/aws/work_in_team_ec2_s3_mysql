resource "aws_db_instance" "aws_mysql_db" {
  allocated_storage    = 10
  #max_allocated_storage = 2000 # upper limit to which Amazon RDS can automatically scale the storage of the DB instance.
  db_name              = join("_", ["TF_db", var.project_short_name])
  engine               = "mysql"
  engine_version       = "5.7"
  instance_class       = "db.t3.micro"
  username             = "foo"
  password             = "Gos2NeeS9Mio3v"
  parameter_group_name = "default.mysql5.7"
  skip_final_snapshot  = true
}