terraform {
  backend "s3"{
    region = "eu-central-1"
    bucket = "terraform-s3-for-statefile"
    key = "global/s3/terraform.tfstate"
    dynamodb_table = "aws_dynamodb_state_locking"
    encrypt = true
  }
}