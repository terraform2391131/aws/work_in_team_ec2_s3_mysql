# What's included:
# EC2 instance
# Mysql database
# S3bucket for terraform.tfstate
# terraform.tfstate remote file locking


# Whats need to be set in this project:
project_short_name = "filters" #small letters and only semicolon, your every project need to have differnet name
aws_region = "eu-central-1"
################ EC2 Settigns #####################
ec2_instance_type = "t3.micro"
ec2_instance_ami = "ami-061afb021813a8927"
################ Database     #####################
# configure your desired database in aws_mysql_db.tf file
################
# configure region in "aws_terraform_backend.tf"
# Thats all!


# Before first use terraform needs first to create S3 bucket for backend,
# so you need first to comment all "aws_terraform_backend.tf" file and run
# terraform init
# terraform apply

# Later to remove this resource you have to:
# comment all "aws_terraform_backend.tf" file
# terraform init
# terraform apply
# you are again with local terraform.tfstate, but you need to clear S3 bucket:
# comment all lifecycle in "aws_s3.tf" file
# comment "aws_s3_bucket_versioning" resource 
# add/uncomment "force_destroy = true" in "aws_s3.tf" file
# terraform apply
# terraform destroy