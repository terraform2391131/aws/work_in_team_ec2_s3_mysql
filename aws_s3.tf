# S3 bucket for state file to work in team
resource "aws_s3_bucket" "s3_state_bucket" {
  bucket = "terraform-s3-for-statefile"

  # force_destroy = true

  lifecycle {
    prevent_destroy = true
  }

  tags = {
    Name        = "Bucket for terraform statefile"
  }
}

resource "aws_s3_bucket_versioning" "s3bv" {
  bucket = aws_s3_bucket.s3_state_bucket.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_server_side_encryption_configuration" "s3bssec" {
  bucket = aws_s3_bucket.s3_state_bucket.id

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}



# other S3 bucket for work stuff
# resource "aws_s3_bucket" "s3_work_bucket" {
#   bucket = join("-", ["terraform-state-", var.project_short_name])
#   #count = 1

#   #force_destroy = true

#   # lifecycle {
#   #   prevent_destroy = true
#   # }

#   tags = {
#     Name        = "Bucket for work stuff"
#   }
# }
# output "s3_files_url" {
#   value = "https://${aws_s3_bucket.s3_work_bucket.bucket}.s3.amazonaws.com/..."
#   description = "S3 files URL."
# }